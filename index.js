const url = 'https://rickandmortyapi.com/api/character';
const container = document.getElementById('characters');
let cards = []
function fetchCards(){
    fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;

        return rawData.map((character, index) => {
			
          //all needed data is listed below as an entity 
           let created = character.created;
           let species = character.species ;
           let img = character.image;
           let episodes = character.episode;
           let name = character.name;
           let location = character.location;
		   let countOfEpisodes = character.episode.length;
            //create element
			const cardTemplate = `	  
            <li data-character="${index}" class="characters-list-item">
                <img class="characters-picture" src="${img}">
                <p class="characters-name">${name}</p>
                <p class="characters-species">${species}</p>
                <p class="characters-location">${location.name}</p>
                <time class="characters-created">${created}</time>
                <p class="characters-episode-length">${countOfEpisodes}</p>
                <ul class="characters-episodes-list">
                    ${episodes.map(function(elmt){
                        return `<li>${elmt}</li>`}).join('')}
                </ul>
                <button class="character-remove" type="button" onclick="removeUserCardByUsername('${name}')">
                    <i class="fas fa-trash-alt" ></i>
                </button>
			</li>
			`
            //append element
            cards.push(cardTemplate);
        });
    })
    .then(() => showOnDom(cards.slice().splice(0,10)))
    .catch((error) => {
        console.log(JSON.stringify(error));
    });
}

function showOnDom(items) {
    for (var i=0; i<items.length; i++) {
        container.innerHTML += items[i];
    }
}

fetchCards();

let allCards = false;
var main = document.getElementById("main");
function drawRest() {
    const documentWidth = document.documentElement.clientWidth/2;
    const documentHeight = document.documentElement.clientHeight - 10;
    const element = document.elementFromPoint(documentWidth, documentHeight);
    if (element.dataset.character === '9') {
        if (!allCards) {
            showOnDom(cards.slice().splice(10));
            allCards = true;
        }
    }
}

document.addEventListener("scroll", drawRest);


function reset () {
    container.innerHTML = "";
    showOnDom(cards.slice().splice(0,10));
    scrollTo(0,0);
}

buttonReset.addEventListener('click',reset);


function sortByType(type) {
    // get elements
    var items = getElementsFromDom();
    
    if (type == 0) {
        items = sortDateAsc(items);
    }
    else if (type == 1) {
        items = sortDateDesc(items);
    }
    else if (type == 2) {
        items = sortByEpisodesAsc(items);
    }
    else if (type == 3){
        items = sortByEpisodesDesc(items);
    }
    
    // clean container
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }

    // append to dom
    for (i = 0; i < items.length; ++i) {
        container.appendChild(items[i]);
      }
}

function sortDateAsc(items) {
    items.sort(function(a,b) {
        if (a.children[4].innerHTML > b.children[4].innerHTML) {
            return 1;
        }
        else {
            return -1;
        }  
      });
    return items;
}

function sortDateDesc(items) {
    items.sort(function(a,b) {
        if (a.children[4].innerHTML < b.children[4].innerHTML) {
            return 1;
        }
        else {
            return -1;
        }  
      });
    return items;
}

function sortByEpisodesAsc(items) {
    items.sort(function(a,b) {
        if (a.children[5].innerHTML == b.children[5].innerHTML) {
            if (a.children[4].innerHTML < b.children[4].innerHTML) {
                return 1;
            }
            else {
                return -1;
            }
        }
        else if (a.children[5].innerHTML < b.children[5].innerHTML) {
            return 1;
        }
        else {
            return -1;
        }
    });
    return items;
}

function sortByEpisodesDesc(items) {
    items.sort(function(a,b) {
        if (a.children[5].innerHTML == b.children[5].innerHTML) {
            if (a.children[4].innerHTML > b.children[4].innerHTML) {
                return 1;
            }
            else {
                return -1;
            }
        }
        else if (a.children[5].innerHTML > b.children[5].innerHTML) {
            return 1;
        }
        else {
            return -1;
        }
    });
    return items;
}

function getElementsFromDom() {
    var items = container.childNodes;
    var itemsArr = [];
    for (var i in items) {
        if (items[i].nodeType == 1) { // get rid of the whitespace text nodes
            itemsArr.push(items[i]);
        }
    }
    return itemsArr;
}

function removeUserCardByUsername(username) {  
    for ( var i=0; i<container.childNodes.length; i++) {
        if (container.childNodes[i].nodeType == 1) {
            if (container.childNodes[i].children[1].innerHTML == username) {
                container.removeChild(container.childNodes[i]);
            }
        }
    }
}

function sortByDateUp () {
     sortByType(0);
}

dateUp.addEventListener('click', sortByDateUp);

function sortByDateDown () {
    sortByType(1);
}

dateDown.addEventListener('click', sortByDateDown);

function sortByEpisodeUp () {
    sortByType(2);
}

episodeUp.addEventListener('click', sortByEpisodeUp);

function sortByEpisodeDown () {
   sortByType(3);
}

episodeDown.addEventListener('click', sortByEpisodeDown);